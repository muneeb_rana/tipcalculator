package com.example.tipcalculatoractivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var tipPercentage = 0.0
        var tip = 0.0
        var total = 0.0

        radioButton.setOnClickListener {
            tipPercentage = 0.10
        }

        radioButton2.setOnClickListener {
            tipPercentage = 0.20
        }

        radioButton3.setOnClickListener {
            tipPercentage = 0.30
        }

        button.setOnClickListener {
            val amt = editText.text.toString()
            if(amt.length == 0 || radioNotChecked()) {
                textView4.text = "Please make sure that you entered a check amount and selected a tip percentage."
            }
            else {
                var amount = editText.getText().toString().toDouble()
                tip = calculateTip(amount, tipPercentage)
                val tipStr = String.format("%.2f", tip)
                total = calculateTotal(amount, tipPercentage)
                val totalStr = String.format("%.2f", total)
                textView4.text = "Your calculated tip is $" + tipStr + " and your total is $" + totalStr
            }

        }

    }

    fun calculateTip(amount: Double, percentage: Double): Double{
        return amount*percentage;
    }

    fun calculateTotal(amount: Double, percentage: Double): Double{
        return amount*percentage + amount;
    }

    fun radioNotChecked(): Boolean {
        var res: Boolean = false
        if (!radioButton.isChecked() && !radioButton2.isChecked() && !radioButton3.isChecked()) {
            res = true
        }
        return res
    }

}
